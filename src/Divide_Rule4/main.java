package Divide_Rule4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import Divide_Rule2.MyCallable;

public class main {

	
	
	
	
	
	public static void main(String[] args) {

		
		Random r = new Random();
		
		// implementa un programa que crei una llista d�N valors enters aleatoris
		ArrayList<Integer> listanumeros = new ArrayList<Integer>();
		for (int i = 0; i < 100000; i++) {
			listanumeros.add(r.nextInt(1000000));
		}
		
		
		
		System.out.println("			-- ACTIVIDAD 1 --			");
		System.out.println();
		AcUno(listanumeros);
		System.out.println();
		System.out.println("_________________________________________");
		System.out.println();
		System.out.println("			-- ACTIVIDAD 2 --			");
		System.out.println();
		AcDos(listanumeros);
		System.out.println();
		System.out.println("_________________________________________");
		System.out.println();
		System.out.println("			-- ACTIVIDAD 3 --			");
		System.out.println();
		AcTres(listanumeros);
		System.out.println();
		System.out.println("_________________________________________");
		System.out.println();
		
	}
	
	

	
	public static void AcUno(ArrayList<Integer> a) {
		float tiempo = 0;
		
		float tiempoDos = 0;
		
		
		ArrayList<Integer> m_unoList = a;
		
		// A continuaci� desar� el seu instant de temps actual en nanosegons 
				try {
					tiempo = System.nanoTime();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				
				ExecutorService executor = Executors.newFixedThreadPool(1);
				Future<List<Integer>> resultat = executor.submit(new MyCallable(m_unoList));
				
				// no acepta mas cosas
				executor.shutdown();
				
				try {	
					System.out.println("El resultat s:" + resultat.get());
					try {
						tiempoDos = System.nanoTime();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				} 
				catch (ExecutionException e) {
					e.printStackTrace();
				}

				
				System.out.println((tiempoDos - tiempo)/100000000);
				
	}
	
	public static void AcDos(ArrayList<Integer> a) {
		float tiempo = 0;
		
		float tiempoDos = 0;
		
		
		ArrayList<Integer> m_dosList = a;
		// A continuaci desar el seu instant de temps actual en nanosegons 
				try {
					tiempo = System.nanoTime();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				
				List<Integer> primera_parte =  m_dosList.subList(0, (int)m_dosList.size()/2);
				List<Integer> segunda_parte =  m_dosList.subList(((int)a.size()/2), (int)m_dosList.size());
				
				
				ExecutorService executor = Executors.newFixedThreadPool(1);
				Future<List<Integer>> resultat = executor.submit(new MyCallable(primera_parte));
				Future<List<Integer>> resultatdos = executor.submit(new MyCallable(segunda_parte));
				
				// no acepta mas cosas
				executor.shutdown();
				
				
				try {	
					System.out.println("El resultat primera mita:" + resultat.get());
					System.out.println("El resultat segunda mita:" + resultatdos.get());
					
					ArrayList<Integer> nueva = new ArrayList<Integer>();
					
					for (int i = 0; i < resultat.get().size(); i++) {nueva.add(resultat.get().get(i));}
					for (int i = 0; i < resultatdos.get().size(); i++) {nueva.add(resultatdos.get().get(i));}
					
					int max = 0;
					int min = 1000000;
					
					 for (int i = 0; i < nueva.size(); i++) {
				            if (nueva.get(i) > max) {
				                max = nueva.get(i);
				            }
				            if (nueva.get(i) < min) {
				                min = nueva.get(i);
				            }
				        }
					
					 System.out.println();
					 System.out.println(nueva);
					 System.out.println();
					 System.out.println("El numero grande es: " + max + "				|				El numero pequenyo es: " + min);
					
					try {
						tiempoDos = System.nanoTime();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				} 
				catch (ExecutionException e) {
					e.printStackTrace();
				}

				
				System.out.println((tiempoDos - tiempo)/100000000);
				
	}
	
	public static void AcTres(ArrayList<Integer> a) {
		float tiempo = 0;
		
		float tiempoDos = 0;
		
		ArrayList<Integer> m_tresList = a;
		
				try {
					tiempo = System.nanoTime();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			
				ExecutorService executor = Executors.newFixedThreadPool(1);

				ArrayList<Future<List<Integer>>> m_arrayFuture = new ArrayList<Future<List<Integer>>>();
				
				int dividir = m_tresList.size() / Runtime.getRuntime().availableProcessors();
				

				for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {

					Future<List<Integer>> resultatArray = executor.submit(new MyCallable(m_tresList.subList(i*dividir, (i+1)*dividir)));
					
					m_arrayFuture.add(resultatArray);
					
				}

				
				// no acepta mas cosas
				executor.shutdown();
				
				
				try {	

					
					ArrayList<Integer> nueva = new ArrayList<Integer>();
					
					for (int i = 0; i < m_arrayFuture.size(); i++) {
						for(int j = 0;j < m_arrayFuture.get(i).get().size();j++) {
							nueva.add(m_arrayFuture.get(i).get().get(j));
						}
						
					}
					
					int max = 0;
					int min = 1000000;
					
					 for (int i = 0; i < nueva.size(); i++) {
				            if (nueva.get(i) > max) {
				                max = nueva.get(i);
				            }
				            if (nueva.get(i) < min) {
				                min = nueva.get(i);
				            }
				        }
					
					 System.out.println();
					 System.out.println(nueva);
					 System.out.println();
					 System.out.println("El numero grande es: " + max + "	|	El numero pequenyo es: " + min);
					
					 
					 
					try {
						tiempoDos = System.nanoTime();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				} 
				catch (ExecutionException e) {
					e.printStackTrace();
				}

				
				System.out.println((tiempoDos - tiempo)/100000000);
	}
	
}
