package Divide_Rule3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class main {

	
	
	
	
	
	public static void main(String[] args) {
		float tiempo = 0;
		
		float tiempoDos = 0;
		
		Random r = new Random();
		
		// implementa un programa que crei una llista d�N valors enters aleatoris
		ArrayList<Integer> listanumeros = new ArrayList<Integer>();
		for (int i = 0; i < 100000; i++) {
			listanumeros.add(r.nextInt(1000000));
		}
		
		// A continuaci� desar� el seu instant de temps actual en nanosegons 
		try {
			tiempo = System.nanoTime();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//List<Integer> primera_parte =  listanumeros.subList(0, (int)listanumeros.size()/2);
		//List<Integer> segunda_parte =  listanumeros.subList(((int)listanumeros.size()/2), (int)listanumeros.size());
		
		
		ExecutorService executor = Executors.newFixedThreadPool(1);
		//Future<List<Integer>> resultat = executor.submit(new MyCallable(primera_parte));
		//Future<List<Integer>> resultatdos = executor.submit(new MyCallable(segunda_parte));
		
		ArrayList<Future<List<Integer>>> m_arrayFuture = new ArrayList<Future<List<Integer>>>();
		
		int dividir = listanumeros.size() / Runtime.getRuntime().availableProcessors();
		

		for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {

			Future<List<Integer>> resultatArray = executor.submit(new MyCallable(listanumeros.subList(i*dividir, (i+1)*dividir)));
			
			m_arrayFuture.add(resultatArray);
			
		}
		
		
		/*
		Runtime rt = Runtime.getRuntime();
		float noentiendo = Runtime.getRuntime().totalMemory();
		*/
		
		// no acepta mas cosas
		executor.shutdown();
		
		
		try {	

			
			ArrayList<Integer> nueva = new ArrayList<Integer>();
			
			for (int i = 0; i < m_arrayFuture.size(); i++) {
				for(int j = 0;j < m_arrayFuture.get(i).get().size();j++) {
					nueva.add(m_arrayFuture.get(i).get().get(j));
				}
				
			}
			
			int max = 0;
			int min = 1000000;
			
			 for (int i = 0; i < nueva.size(); i++) {
		            if (nueva.get(i) > max) {
		                max = nueva.get(i);
		            }
		            if (nueva.get(i) < min) {
		                min = nueva.get(i);
		            }
		        }
			
			 System.out.println();
			 System.out.println(nueva);
			 System.out.println();
			 System.out.println("El numero grande es: " + max + "	|	El numero pequenyo es: " + min);
			
			 
			 
			try {
				tiempoDos = System.nanoTime();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		} 
		catch (ExecutionException e) {
			e.printStackTrace();
		}

		
		System.out.println((tiempoDos - tiempo)/100000000);
		
		
		

		
		
		
	}
	
}
