package Divide_Rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<ArrayList<Integer>> {

	ArrayList<Integer> m_lista;
	
	public MyCallable(ArrayList<Integer> a){
		m_lista = a;
	}
	
	
	
	@Override
	public ArrayList<Integer> call() throws Exception {
	
        int max = 0;
        int min = 1000000000;
        
        for (int i = 0; i < m_lista.size(); i++) {
            if (m_lista.get(i) > max) {
                max = m_lista.get(i);
            }
            if (m_lista.get(i) < min) {
                min = m_lista.get(i);
            }
        }
        
        ArrayList<Integer> numeros = new ArrayList<Integer>();
        numeros.add(max);
        numeros.add(min);
        
		
		return numeros;
	}

	
}